ics-ans-role-proxyclients
===================

Ansible role to install proxy client settings.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
proxyclient_url: "http://proxy:8080"
proxyclient_no_proxy:
  - 127.0.0.1
  - localhost
  - host.ess.eu

```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-proxyclient
```

License
-------

BSD 2-clause
