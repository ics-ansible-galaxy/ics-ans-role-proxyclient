import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('disabled')


def test_env(host):
    profile_proxy = host.file('/etc/environment').content_string.splitlines()
    assert 'http_proxy=http://proxy:8080' not in profile_proxy
    assert 'proxy=http://proxy:8080' not in profile_proxy
    assert 'ftp_proxy=http://proxy:8080' not in profile_proxy
    assert 'https_proxy=http://proxy:8080'not in profile_proxy


def test_package_manager(host):
    if host.ansible("setup")["ansible_facts"]["ansible_distribution"] == "CentOS" or host.ansible("setup")["ansible_facts"]["ansible_distribution"] == "Concurrent CPU SDK":
        conf_file = "/etc/yum.conf"
        if "ess_linux" in host.ansible.get_variables()["group_names"]:
            conf_file = "/etc/dnf/dnf.conf"
        yumconf = host.file(conf_file).content_string.splitlines()
        assert 'proxy = http://proxy:8080' not in yumconf
    else:
        conf_file = "/etc/apt/apt.conf.d/80proxy"
        aptconf = host.file(conf_file).content_string.splitlines()
        assert 'Acquire::http::Proxy' not in aptconf
